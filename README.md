The Universal Declaration of Human Rights:

    Article 26

        Everyone has the right to education. Education shall be free, at least in the elementary and fundamental stages. Elementary education shall be compulsory. Technical and professional education shall be made generally available and higher education shall be equally accessible to all on the basis of merit.
        Education shall be directed to the full development of the human personality and to the strengthening of respect for human rights and fundamental freedoms. It shall promote understanding, tolerance and friendship among all nations, racial or religious groups, and shall further the activities of the United Nations for the maintenance of peace.
        Parents have a prior right to choose the kind of education that shall be given to their children.

Indian Constitution and Indian Acts:

    The Constitution (Eighty-sixth Amendment) Act, 2002 inserted Article 21-A in the Constitution of India to provide free and compulsory education of all children in the age group of six to fourteen years as a Fundamental Right in such a manner as the State may, by law, determine. The Right of Children to Free and Compulsory Education (RTE) Act, 2009, which represents the consequential legislation envisaged under Article 21-A, means that every child has a right to full time elementary education of satisfactory and equitable quality in a formal school which satisfies certain essential norms and standards.